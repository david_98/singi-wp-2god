from flaskext.mysql import MySQL # Importovanje klase za rad sa MySQL serverom.
from flaskext.mysql import pymysql # Importovanje paketa za podesavanje tipa kursora.

# Instanciranje objekta za upravljanje konekcijama sa bazom podataka.
#inace bi bilo ovako 
# mysql = MySQL(app, cursorclass=pymysql.cursors.DictCursor) , sa app ali ne ide jer se ovaj deo ubacuje gde vec treba i tu gde se ubaci se onda mora dodati mysql.init_app(referenca na flask objekat)
mysql = MySQL(cursorclass=pymysql.cursors.DictCursor) # on ovde bez app za prvi argument ne zna sa kim da se poveze tj kome radi to konekcije tako da se tamo gde se ubacuje mora dodati mysql.init_app()
# mysql2 = MySQL() # to da je app prvi argument za inicijalizator moze se videti kad kliknemo ctrl nad MySQL klasom pa klik i vidi se u klsi da se app iz inicijalizatora daje u metodi init_app(self, app)