import os
import flask
from flask import Flask, request
from flask.blueprints import Blueprint
# za access token (jwt)
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity, get_jwt_claims

# za mysql
from utils.db import mysql

blog_blueprint = Blueprint("blog_blueprint", __name__)

@blog_blueprint.route("/categories")
def getCategories(): 
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM category")
    categories = cursor.fetchall()
    return flask.jsonify(categories), 200

# ------- ZA NOVI BLOG ---------  
@blog_blueprint.route("/new_blog", methods=["POST"])
@jwt_required
def addNewBog():
    claims = get_jwt_claims()
    blog = request.json
    blog['authors_id'] = claims['id']
    # blog['date'] = datetime.datetime.now()

    db = mysql.get_db()
    cursor = db.cursor()
    # try:
    cursor.execute("INSERT INTO blog(state, authors_id, headline, category_id, score_total, score_number, views, date) VALUES(%(state)s, %(authors_id)s, %(headline)s, %(category_id)s, %(score_total)s, %(score_number)s, %(views)s, %(date)s)", blog)
    # except:
    #     print("greska")

    db.commit()
    # dobavljam potrebne informacije za popunjavanje tabele content
    # tj. blog id i autora
    # contentData = {}
    cursor.execute("SELECT id FROM blog WHERE authors_id=%s AND headline=%s", (blog['authors_id'], blog['headline'], ))
    blog_id = cursor.fetchone()
    id = blog_id["id"]
    # contentData.author = claims['author']
    return flask.jsonify(id), 200

@blog_blueprint.route("/new_blog/text", methods=["POST"])
@jwt_required
def addNewContentText():
    db = mysql.get_db()
    cursor = db.cursor()

    blog_id = request.json['blog_id'] 
    blog_text = request.json['contentText']
    contentName = str(blog_id) + ".txt"
    cursor.execute("INSERT INTO content(blog_id, name) VALUES(%s, %s)", (blog_id, contentName, ))
    db.commit()

   
    # kreiranje i dodavanje tekstualnog fajla u static/texts
    dest = "static/texts/" + contentName
    with open(dest, "w") as file: 
        file.write(blog_text)

    return "", 200

@blog_blueprint.route("/new_blog/media", methods=["POST"])
@jwt_required
def addNewContentMedia():
    db = mysql.get_db()
    cursor = db.cursor()

    blog_id = request.headers["blog_id"]
    # kreiranje i dodavanje media u static/media
    mediaName = blog_id + "." + request.files["picture"].filename.split(".")[1]
    # mediaName = request.files["picture"].filename

    request.files["picture"].save("static/media/" + mediaName + "", buffer_size=16384)

    blog_id_int = int(blog_id)
    cursor.execute("INSERT INTO content(blog_id, name) VALUES(%s, %s)", (blog_id_int, mediaName, ))
    db.commit()

    return "", 200

# dobavljanje bloga za klijenta
@blog_blueprint.route("/blog/<int:id>")
def blog_fetching(id):
    print("usao")
    db = mysql.get_db()
    cursor = db.cursor()
    # dobavljanje bloga
    cursor.execute("SELECT * FROM blog WHERE id=%s", (id, ))
    blog = cursor.fetchone()
    # dobavljanjne naziva media i tekstualnog fajla
    cursor.execute("SELECT * FROM content WHERE blog_id=%s", (id, ))
    results = cursor.fetchall()
    first_result = results[0]
    second_result = results[1]
    text = ""
    media_name = ""
    # provera da li je media ili txt i dobavljanje
    if "txt" in first_result["name"]: 
        # za tekst
        dest = "static/texts/" + first_result["name"]
        with open(dest, "r") as reader: 
            text = reader.read()
        # za media
        media_name = "media/" + second_result["name"]
    else:
        media_name = "media/" + first_result["name"]
        dest = "static/texts/" + second_result["name"]
        with open(dest, "r") as reader: 
            text = reader.read()

    print(media_name)
    
    # kreiranje i slanje odgovora
    
    return flask.jsonify({"blog": blog, "imgName": media_name, "blogText": text})

# azuriranje bloga
@blog_blueprint.route("/edit/save_changes", methods=["POST"])
@jwt_required
def update_blog():
    db = mysql.get_db()
    cursor = db.cursor()
    # ?? DA LI TREBA da uzmem id iz tokena i da ga proverim sa id autora iz bloga ciji id vidim iz body-a
    cursor.execute("UPDATE blog SET state=%(state)s, headline=%(headline)s, date=%(date)s WHERE id=%(id)s", request.json)
    db.commit()

    blog_id = request.json["id"]
    contentText = request.json["contentText"]
    # vrsim prepis preko starog txt fajla
    dest = "static/texts/" + str(blog_id) + ".txt"
    with open(dest, 'w') as textFile: 
        textFile.write(contentText)
    
    return "", 200

# azuriranje media bloga                                    
@blog_blueprint.route("/edit/change_media", methods=["POST"])
@jwt_required
def update_media():
    blog_id = request.headers['blog_id']

    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM content WHERE blog_id=%s", (blog_id, ))
    results = cursor.fetchall()
    first_result = results[0]
    second_result = results[1]
    old_name = ""
    if "txt" in first_result["name"]: 
        # znaci da je drugi rezultat content sa imenom medie koji sad brisemo i stavljamo novu
        old_name = second_result["name"]
        path = "static/media/" + old_name
        os.remove(path)
    else:
        old_name = first_result["name"]
        path = "static/media/" + old_name
        os.remove(path)
    new_name = blog_id + "." + request.files["picture2"].filename.split(".")[1]
    path = "static/media/" + new_name
    request.files["picture2"].save(path, buffer_size=16384)
    cursor.execute("UPDATE content SET name=%s WHERE name=%s", (new_name, old_name, ))
    db.commit()

    return "", 200

@blog_blueprint.route("/author/<int:id>", methods=["GET"])
def get_author(id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM author WHERE id=%s", (id, ))
    result = cursor.fetchone()
    
    return flask.jsonify(result), 200

@blog_blueprint.route("/addGrade/<int:id>", methods=["PATCH"])
def update_blog_score(id):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM blog WHERE id=%s", (id, ))
    result = cursor.fetchone()
    old_score_total = result["score_total"]
    new_score_number = result["score_number"] + 1
    new_score_total = old_score_total + int(request.json["grade"])
    cursor.execute("UPDATE blog SET score_total=%s, score_number=%s WHERE id=%s", (new_score_total, new_score_number, id, ))
    db.commit()

    return "", 200

@blog_blueprint.route("/views_update", methods=["PATCH"])
def update_views():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE blog SET views=%(views)s WHERE id=%(id)s", request.json)
    db.commit()
    print("trazi")
    return "", 200