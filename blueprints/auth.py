import flask
from flask import Flask, request
from flask.blueprints import Blueprint
# za access token (jwt)
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity

# za mysql
from utils.db import mysql

auth_blueprint = Blueprint("auth_blueprint", __name__)


# ---  sigh in -----
# kreiranje access token-a prilikom logovanja
@auth_blueprint.route("/auth/signIn", methods=["POST"])
def signin():
    # hvatamo ako nije postat json
    if not request.is_json:
        return flask.jsonify({"msg": "Missing JSON in request."}), 400 # los zahtev
    
    # to je flask.request ali je importovan request iz flaska
    author = request.json.get("author", None)
    password = request.json.get("password", None)

    if not author:
        return flask.jsonify({"mgs": "Missing username parametar"}), 400
    if not password: 
        return flask.jsonify({"msg": "Missing password parametar"}), 400
    
    if author == "test" or password == "test":
        return flask.jsonify({"msg": "Easy to guess."}), 400

    # dodaje korisnika u bazu
    db = mysql.get_db()
    cursor = db.cursor()
    # provera da li korisnik sa tim usernameom postoji
    cursor.execute("SELECT * FROM author WHERE author=%(author)s", request.json)
    rezultat = cursor.fetchone()
    if rezultat:
        return "Taken username", 200
    else:
        request.json["profPic"] = "profilePictures/defaultProfPic.jpg"
        cursor.execute("INSERT INTO author(author, password, profPic) VALUES(%(author)s, %(password)s, %(profPic)s)", request.json)
        db.commit()

    # napomena meni:
    # create_access_token(identity, fresh=False, expires_delta=None, user_claims=None, headers=None)[source]
    # username je ok da bude, u token se ne bi trebali stavljati osetljivi podaci poput passworda jer ako se i procita token, kad se salje zahtev zasticenj ruti salje se i token a
    # da bi ga neko "vestacki" kreiaro mora pored onoga sto ide u token da zna i tajni kljuc. 
    # Ako bi password bio u tokenu i on ga procita onda bi prosao u loginu, ne bi ni morao da zna tajni kljuc
    token = { 'access_token': create_access_token(author) }
    return flask.jsonify(token), 200


# --- log in  -----
@auth_blueprint.route("/auth/logIn", methods=["POST"])
def logIn():
    # hvatamo ako nije postat json
    if not request.is_json:
        return flask.jsonify({"msg": "Missing JSON in request."}), 400 # los zahtev
    
    # to je flask.request ali je importovan request iz flaska
    author = request.json.get("author", None)
    password = request.json.get("password", None)

    if not author:
        return flask.jsonify({"msg": "Missing author parametar"}), 400
    if not password: 
        return flask.jsonify({"msg": "Missing password parametar"}), 400

    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM author WHERE author=%(author)s AND password=%(password)s", request.json)
    rezultat = cursor.fetchone()
    if rezultat:
        token = { 'access_token': create_access_token(author) }
        return flask.jsonify(token), 200
    else:
        return "No match", 200

@auth_blueprint.route("/vratiToken", methods=["POST"])
def vratiToken():
    
    return flask.jsonify(request.headers["Authorization"])