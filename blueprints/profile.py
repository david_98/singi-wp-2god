import os
import flask
from flask import Flask, request
from flask.blueprints import Blueprint
# za access token (jwt)
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity, get_jwt_claims

# za mysql
from utils.db import mysql

profile_blueprint = Blueprint("profile_blueprint", __name__)

@profile_blueprint.route("/profile_page/validity", methods=["GET"])
@jwt_required
def check_author_validity():
    print("validity")
    claims = get_jwt_claims()
    # iz claimsa dobijamo tog tipa kojeg smo i stavili u njega tj int u ovom slucaju a iz headersa u str, 2 != "2" zato mora konverzija
    id_from_token = str(claims["id"])
    authors_id = request.headers["authorsId"]

    if id_from_token == authors_id:
        return flask.jsonify({"validity": "valid"})
    else:
        return flask.jsonify({"validity": "not_valid"})

@profile_blueprint.route("/profile_page/data/<int:authors_id>/<status>", methods=["GET"])
def get_data_for_profile(authors_id, status):
    print("data")
    db = mysql.get_db()
    cursor = db.cursor()
    
    cursor.execute("SELECT * FROM author WHERE id=%s", (authors_id, ))
    author_info = cursor.fetchone()
    cursor.execute("SELECT * FROM blog WHERE authors_id=%s AND state=1", (authors_id, ))
    published_blogs = cursor.fetchall()

    if status == "valid":
        cursor.execute("SELECT * FROM blog WHERE authors_id=%s AND state=0", (authors_id, ))
        unpublished_blogs = cursor.fetchall()
        return flask.jsonify({"authorInfo": author_info, "publishedBlogs": published_blogs, "unpublishedBlogs": unpublished_blogs})
    
    return flask.jsonify({"authorInfo": author_info, "publishedBlogs": published_blogs})


@profile_blueprint.route("/profile/edit", methods=["PATCH"])
@jwt_required
def save_changes():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("UPDATE author SET author=%(author)s, about=%(about)s WHERE author=%(author)s", request.json)
    db.commit()
    return "", 200


@profile_blueprint.route("/profile/edit_profile_picture", methods=["PATCH"])
@jwt_required
def change_profile_image():
    claims = get_jwt_claims()
    print("sad")
    print(claims)
    author = str(claims['author'])
    # brisanje stare slike
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM author WHERE author=%s", (author, ))
    result = cursor.fetchone()
    imagePath = result["profPic"]
    if imagePath != "profilePictures/defaultProfPic.jpg":
        os.remove("static/" + imagePath)
    
    # cuvanje nove prfilne slike i update na bazi
    newImageName = request.files["newImage"].filename
    newImagePath = "profilePictures/" + newImageName
    print(newImagePath)
    full_path = "static/" + newImagePath
    request.files["newImage"].save(full_path, buffer_size=16384)
    cursor.execute("UPDATE author SET profPic=%s WHERE author=%s", (newImagePath, author, ))
    db.commit()

    return flask.jsonify(newImagePath), 200
    
@profile_blueprint.route("/profile/categories", methods=["GET"])
def get_all_categories():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM category")
    result = cursor.fetchall()
    return flask.jsonify(result), 200

@profile_blueprint.route("/profile/remove_blog/<int:id>", methods=["PUT"])
@jwt_required
def remove_blog(id):
    db = mysql.get_db()
    cursor = db.cursor()
    # sklanjanje content i medija
    cursor.execute("SELECT * FROM content WHERE blog_id=%s", (id, ))
    results = cursor.fetchall()
    for i in range(len(results)):
        result = results[i]
        if result["name"].split(".")[1] == "txt":
            os.remove("static/texts/" + result["name"])
        else:
            os.remove("static/media/" + result["name"])
    cursor.execute("DELETE FROM content WHERE blog_id=%s", (id, ))
    db.commit()
    # sklanjanje bloga
    cursor.execute("DELETE FROM blog WHERE id=%s", (id, ))
    db.commit()

    return "", 200