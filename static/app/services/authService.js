(function(angular){
    var app = angular.module("app");
    app.factory('authService', [function(){
        var accessToken = null;
    
        return function(token=null){
            if(token){
                if(token == "remove"){
                    accessToken = null;
                } else {
                    accessToken = token;
                }
                
            } else {
                return accessToken;
            }
        } 
    }])
})(angular)