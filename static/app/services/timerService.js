(function(angular){
    var app = angular.module("app");
    app.factory("timerService", ['authService', '$state', function(authService, $state){
        function startTimer(duration){
            
            logInTimer = window.setTimeout(function(){
                window.localStorage.removeItem("token");
                window.localStorage.removeItem("token_duration");
                authService('remove');
                clearTimer();
            }, duration );
        };
        function clearTimer(){
            if(logInTimer){
                window.clearTimeout(logInTimer);
                $state.go('auth', {mode: 'logIn'});
            }
        }

        return function(duration=3600000){
            startTimer(duration);
        }
    }])
})(angular)