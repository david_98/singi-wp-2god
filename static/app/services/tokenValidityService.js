(function(angular){
    var app = angular.module("app");
    app.factory("tokenValidityService", ['authService', '$state', 'timerService', function(authService, $state, timerService){
        return function(status=null){
            var token = window.localStorage.getItem("token");
            // proveravamo da li je uopste doslo do prijavljivanja
            if(token){
                // provara da li je jos validan
                if(parseInt(window.localStorage.getItem("token_duration")) > Date.now()) {
                    // nije istekao
                    authService(token);
                    // dajemo timeru novo, tj sada umanjeno vreme, vazenja tokena
                    var newTime = parseInt(window.localStorage.getItem("token_duration")) - Date.now();
                    timerService(newTime);
                    return "token valid";
                } else {
                    if(status==null){
                        // istekao, redirekcija na authPage
                        window.localStorage.removeItem("token");
                        window.localStorage.removeItem("token_duration");
                        $state.go('authPage', {mode: 'logIn'});
                        return "token not valid";
                    }

                    
                }
            } else {
                if(status==null){
                    // nije ni bilo prijavljivanja
                    $state.go('auth', {mode: 'logIn'});
                    return "token not valide";
                }
                
            }
        }
    }])
})(angular)