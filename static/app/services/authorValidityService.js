(function(angular){
    var app = angular.module("app");
    app.factory("authorValidityService", ["authService", "$http", '$state', 'tokenValidityService', function(authService, $http, $state, tokenValidityService){
       
        return function(blogId, token){
            // vracam kreirani promise
            return new Promise(function(resolve, reject){
            
                // nepotrebno ali ostavljeno zbog promises chaining primera
                var token2  = token;
                resolve(token2);

            }).then(
                token => {
                    // saljem na validaciju
                    console.log(token);
                    console.log(blogId);
                    $http.get("api/author_validity", {"headers": {"Authorization": "Bearer " + token, "blog_id": blogId}}).then(
                        function(response){
                            if(response.data['access']){
                                return "token validity passed";
                            } else {
                                console.log("vrace false");
                                // throw new Error("token validity failed");
                                $state.go('errorPage', {code: '401'});
                            }
                        }, function(reason){  
                            console.log(reason);
                            // throw new Error("Token validity failed");
                            $state.go('errorPage', {code: '401'});
                        }
                    );
                }
            ).catch(
                () => {
                    console.log("Author validity Promise reject");
                    $state.go('errorPage', {code: '401'});
                }
            )
        }
    }])
})(angular)