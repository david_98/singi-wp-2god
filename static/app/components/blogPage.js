(function(angular, document){
    var app = angular.module("app");
    app.controller("blogController", ['$http', '$stateParams', '$state', 'authService', 'tokenValidityService', 'authorValidityService', function($http, $stateParams, $state, authService, tokenValidityService, authorValidityService){
        var that = this;
        this.stateEdit = false;
        this.stateNew = false;
        this.stateRead = false;
        this.touched = false;
        this.message = false;

        this.categories = [];
        this.blogContent = {
            id: null,
            authors_id: null,
            state: 0,
            headline: 'Headline goes here...',
            contentText: "Blog's content...",
            imgSrc: null,
            views: 0,
            score_total: 0,
            score_number: 0,
            score: 0,
            date: null 
        };
        this.author;
        this.blogId = $stateParams['blog_id']

  

        if(!authService()){
            tokenValidityService("token not needed");
            console.log("usao u validaciju");
        };
        this.token = authService();
        console.log("nastavio u blog ");
        
        // metode slanja
        this.sendContent = function(){
            // provere
            if(that.headline == "" || that.cententText == "" || that.imgSrc == ""){
                that.message = true;
            } else {
                // this.blogContent.author_id = na bekendu
                this.blogContent.date = new Date()
            }
        }

        this.checkState = function(state, blogId=null){

            switch(state){
                case "read_blog":
                    if(blogId && !isNaN(blogId)){
                        
                        that.getBlog(blogId);

                        console.log("ok");
                    } else {
                        $state.go('errorPage', {code: '404'})
                    }
                    break;
                case "edit_blog":
                    if(blogId && !isNaN(blogId)){
                        
                        authorValidityService(blogId, that.token).then(
                            success => {
                                that.stateEdit = true;
                                that.getBlog(blogId);
                                console.log("prosao proveru");
                            }, err => {
                                $state.go('errorPage', {code: '404'});
                            }
                        );
                        console.log("kod posle");
                    } else {
                        $state.go('errorPage', {code: '404'})
                    }
                    
                    break;
                case "new_blog":
                    if(authService()){
                        if(blogId){
                            $state.go('errorPage', {code: '400'})
                        };
                        that.stateNew = true;
                    } else {
                        $state.go('errorPage', {code: '401'})
                    };
                    break;
                default:
                    $state.go('errorPage', {code: '400'})
            }
        }
        // kada je izvrsio neku izmenu
        this.showOptions = function(){
            this.touched = true;
        }
        // na cancel opciju
        this.backToLandingPage = function(){
            $state.go("pocetna");
        }
        // ---- SAVE BLOG -----
        this.saveBlog = function(){
            if(that.stateNew){
                // provera da li je pocetna media izabrana
                if(!document.getElementById("addPicNew").value){
                    that.message = true;
                } else {
                    // pamcenje promene, bez promene stanje
                    that.publishBlog("unpublished");       
                }      
            } else {
                that.updateBlog(); // verovatno sa id bloga
            }
        }
        // ------- za STATE NEW ----------
        this.publishBlog = function(mode=null){
            // znacajno za publish u edit-u
            if(that.stateNew){
                var fromForm = document.getElementById("form");
                var infoToSend = {};
                infoToSend.headline = fromForm['headline'].value;
                infoToSend.category_id = fromForm['category'].value;
                
                if(mode == "unpublished"){
                    infoToSend.state = 0;
                    infoToSend.date = null;
                } else {
                    infoToSend.state = 1;
                    infoToSend.date = new Date().toISOString().slice(0, 19).replace('T', ' ');
                }
                
                infoToSend.views = 0;
                infoToSend.score_total = 0;
                infoToSend.score_number = 0;
                
                var token = authService()
                $http.post("api/new_blog", infoToSend, {headers: {'Authorization': 'Bearer ' + token }}).then(
                    function(response){
                        console.log(response);
                        var blog_id = response.data;
                        console.log("drugi deo na redu");
                        // pozivamo metodu za slanje teksta bloga na bekend
                        that.sendBlogText(blog_id, token);
                    }, function(reason){
                        console.log(reason);
                    }
                )
            } else {
                that.updateBlog("publish");
            }
            
        }
        this.sendBlogText = function(blog_id, token){
            var fromForm = document.getElementById("form");
            var infoToSend = {};
            infoToSend['contentText'] = fromForm['content'].value;
            infoToSend['blog_id'] = blog_id;

            $http.post('api/new_blog/text', infoToSend, {headers: {'Authorization': 'Bearer ' + token}}).then(
                function(response){
                    console.log(response);
                    that.sendBlogMedia(blog_id, token);
                }, function(reason){
                    console.log(reason);
                }
            )
        }

        this.sendBlogMedia = function(blog_id, token){
            let myForm = document.getElementById('form');
            var formData = new FormData(myForm);

            var request = new XMLHttpRequest();
            request.open("POST", "api/new_blog/media");

            id = blog_id + "";
            request.setRequestHeader('blog_id', id);
            request.setRequestHeader("Authorization", "Bearer " + token);
            request.send(formData);
            // redirekcija
            $state.go("pocetna");
        }
        // --------------------------------

        // ------- ZA SAVE OPCIJU --------
        this.updateBlog = function(message=null){
            // provera da li objavljujemo neobjavljeni blog
            if(message == "publish"){
                that.blogContent.date = new Date().toISOString().slice(0, 19).replace('T', ' ');
                that.blogContent.state = 1;
            } else {
                if(that.blogContent.date != null){
                    that.blogContent.date = that.blogContent.date.toISOString().slice(0, 19).replace('T', ' ');
                }
            }

           
            var token = authService();
            $http.post('api/edit/save_changes', that.blogContent, {headers: {'Authorization': 'Bearer ' + token}}).then(
                function(response){
                    console.log(response);
                }, function(reason){
                    console.log(reason);
                }
            )

            // provera da li je autor izabrao novu sliku 
            if(document.getElementById('addPicEdit').value){
                // poziv metode sa slanjem slike
                let myForm = document.getElementById('form');
                var formData = new FormData(myForm);

                var request = new XMLHttpRequest();
                request.open("POST", "api/edit/change_media");

                id = that.blogContent.id + "";
                request.setRequestHeader('blog_id', id);
                request.setRequestHeader("Authorization", "Bearer " + token);
                request.send(formData);
            };
            // redirekcija
            $state.go('pocetna');
        }
        // ----- dobavljanje kategorija ------
        this.getCategories = function(){
            $http.get("api/categories").then(
                function(response){
                    that.categories = response.data;
                }, function(reason){
                    console.log(reason);
                }
            )
        }
        //  ------- dobavljanje bloga za edit i read mode -----
        this.getBlog = function(blogId){
            
            $http.get('api/blog/' + blogId).then(
                function(response){
                    if(response.data.blog.state == 0 && $stateParams["state"] == "read_blog") {
                        $state.go("errorPage", {"code": 400})
                    } else {
                        that.blogContent.id = response.data.blog.id;
                        that.blogContent.authors_id = response.data.blog.authors_id;
                        console.log(that.blogContent.authors_id);
                        that.blogContent.headline = response.data.blog.headline;
                        that.blogContent.state = response.data.blog.state;
                        that.blogContent.views = response.data.blog.views;
                        that.blogContent.score_total = response.data.blog.score_total;
                        that.blogContent.score_number = response.data.blog.score_number;
                        var score = Math.round(that.blogContent.score_total / that.blogContent.score_number);
                        if(isNaN(score)){
                            that.blogContent.score = 0;
                        } else {
                            that.blogContent.score = score;
                        }
                        console.log("Dobavi blog");
                        // vrsimo proveru da li je u pitanju objavljeni blog ili ne 
                        if(that.blogContent.state == 1){
                            // objavljen je i ima datum
                            that.blogContent.date = new Date(response.data.blog.date);
                        } else {
                            // nije objavljen, jos nema datum  
                            that.blogContent.date = null;
                        }
                        // dobavljanje slike
                        that.blogContent.imgSrc = response.data.imgName;
                        // dobavljanje teksta
                        that.blogContent.contentText = response.data.blogText;
                        // dobavljanje autora
                        $http.get("api/author/" + response.data.blog.authors_id).then(
                            function(response){
                                that.author = response.data['author'];
                                console.log(response);
                                // podizanje broja viewsa 
                                console.log("presao");
                                if($stateParams["state"] == "read_blog"){
                                    that.stateRead = true; 
                                    that.updateViews();
                                }
                                
                            }, function(reason){
                                console.log(reason);
                            }
                        )
                    }
                    
                }, function(reason){
                    console.log(reason);
                }
                
            )
            
        }
        this.sendGrade = function(){
            var grade = document.getElementById("gradeField").value;
            $http.patch("api/addGrade/" + that.blogContent.id, {"grade": grade}).then(
                function(response){
                    alert("Submitted.");
                }, function(reason){
                    console.log(reason);
                }
            )
        }
        this.updateViews = function(){
            var blog = {"id": that.blogId, "views": that.blogContent.views + 1};
            $http.patch("api/views_update", blog).then(
                function(response){
                    console.log(response);
                }, function(reason){
                    console.log(reason);
                }
            )
        }

        // ---- pozivi po inicijalizaciji ------
        this.checkState($stateParams['state'], $stateParams['blog_id']);
        this.getCategories();
        


        // to do: 
    

        // zastita:  
        // - sanitacija inputa u edit i new

        // autorizacija: 
        // - na bekendu kad dobije token proverava da li je taj autor autor ciji blog se trazi - sredjeno
        // - provera na komponentama, na pocetku, da li je validan token
        // srediti da se neobjavljeni blogovi ne mogu prikazati na read modu - sredjeno
        // srediti da ase slike za kategoriju cuvaju na klijentu
        // sreiti da nije potrebno logovanje ako zeli samo da cita

        // izgled
        // srediti font i sl gde nije 

        // PITANJA: 
        // da li u service mozemo da stavimo promise i da radimo then
    }])
})(angular, document)