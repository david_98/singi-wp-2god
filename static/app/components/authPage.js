(function(angular){
    var app = angular.module("app");
    app.controller("authController", ['$http', '$state', '$stateParams', 'authService', 'timerService', function($http, $state, $stateParams, authService, timerService){
        var that = this;
        this.signIn = false;
        this.logIn = false;
        this.message = false;
        this.logInTimer = null;
        this.user = {
            author: "",
            password: ""
        }

        this.logInCall = function(){
            $http.post('api/auth/logIn', that.user).then(
                function(response){
                    if(response.data != 'No match'){
                        authService(response.data['access_token']);
                        // postavljam token u localStorage
                        window.localStorage.setItem("token", authService());
                        var tokenDuration = Date.now() + 3600000;
                        window.localStorage.setItem("token_duration", tokenDuration);
                        // pokrecem tajmer
                        timerService();
                        $state.go('pocetna');
                    } else {
                        that.message = true;
                    }
                }, function(reason){
                    alert(reason.data['msg']);
                }
            )
        };

        this.signInCall = function(){
            that.user['profPic'] = "";
            $http.post('api/auth/signIn', that.user).then(
                function(response){
                    if(response.data != "Taken username"){
                        authService(response.data['access_token']);
                        window.localStorage.setItem("token", authService());
                        var tokenDuration = Date.now() + 3600000;
                        window.localStorage.setItem("token_duration", tokenDuration);
                        // pokrecem timer
                        timerService()
                        $state.go('pocetna');
                    } else {
                        that.message = true;
                    }
                }, function(reason){
                    alert(reason.data['msg']);
                }
            )
        };

        this.go = function(){
            if(that.signIn){
                that.signInCall();
            } else 
                that.logInCall();
        }

        this.checkMode = function(){
            var mode = $stateParams['mode'];
            if( mode == 'logIn'){
                that.logIn = true;
                that.signIn = false;
            } else {
                that.logIn = false;
                that.signIn = true;
            }
        }

        

        this.checkMode()
    }])
})(angular)