(function(angular, document){
    var app = angular.module("app");
    app.controller("pocetnaController", ['$http', '$stateParams', 'authService', '$state', 'tokenValidityService', function($http, $stateParams, authService, $state, tokenValidityService){
        var that = this;
        this.token = null;
        this.imgFile = null;
        this.dobavljenaSlika = false;
        this.imgSrc = "";

        // provera validnosti tokena
        tokenValidityService("just watching");
        this.token = authService();

        this.blogs = [];
        this.displayedBlogs = [];
        this.helperArray = [];
        this.content = [];
        this.categories = [];
        this.authors = [];
        this.counter = 3;

        console.log(that.displayedBlogs);
        console.log(that.categories);
        // ---- ZA LOG OUT ----
        this.logOut = function(){
            authService("remove");
            window.localStorage.removeItem("token");
            window.localStorage.removeItem("token_duration");
            $state.go('auth', {mode: 'logIn'});
        };

        // ---- OKO BLOGOVA, PRIKAZA, PRETRAGE... ---
        this.getInitialContent = function(){
            $http.get("api/get_initial_content").then(
                function(response){
                    that.blogs = response.data["blogs"];
                    that.helperArray = that.blogs;
                    that.content = response.data["content"];
                    that.categories = response.data["categories"];
                    that.authors = response.data['authors'];
                    // za prikazane blogove
                    that.firstThreeBlogs();
                     
                }, function(reason){
                    console.log(reason);
                }
            )
        };

        this.getScore = function(total, number){
            var score = Math.round(total/number);
            if (isNaN(score)){
                score = 0;
            };
            return score;
        };
        this.getAuthor = function(id){
            for (author of that.authors){
                if (author["id"] == id){
                    return author["author"];
                }
            };
            return;
        };

        this.displayMore = function(){
            var i = that.displayedBlogs.length;
            var j = i + 3;
            while(i<j){
                if(that.helperArray[i]){
                    that.displayedBlogs.push(that.blogs[i]);
                };
                i += 1;
            };
        };

        this.search = function(type, additional=null){
            switch(type){
                case "searchField":
                    var request = document.getElementById('searchField').value;
                    that.helperArray = [];
                    for(blog of that.blogs){
                        if(blog["headline"].toLowerCase().includes(request.toLowerCase())){
                            that.helperArray.push(blog);
                        }
                    };

                    that.firstThreeBlogs();
                    break;
                case 'category':
                    var request = additional;
                    that.helperArray = [];
                    for(blog of that.blogs){
                        if(that.categories[blog['category_id']-1]['category_name'] == request){
                            that.helperArray.push(blog);
                        }
                    };

                    that.firstThreeBlogs();
                    break;
                case 'sortBy': 
                    that.helperArray = [];
                    $http.get("api/get_initial_content/" + additional).then(
                        function(response){
                            that.helperArray = response.data;

                            that.firstThreeBlogs();
                        }, function(reason){
                            console.log(reason);
                        }
                    );
                    break;
            }

        };

        this.getAuthorPic = function(blog){
            for(author of that.authors){
                if (author['id'] == blog['authors_id']){
                    return author['profPic'];
                }
            }
        };

        this.getCoverPic = function(blog){
            for(content of that.content){
                if(content['blog_id'] == blog['id']){
                    if(!content['name'].includes("txt")){
                        return "media/" + content['name'];
                    }
                }
            }
        };

        this.firstThreeBlogs = function(){
            var i = 0;
            if(that.displayedBlogs.length != 0){
                that.displayedBlogs = [];
            };
            
            while(i<3){
                if(that.helperArray[i] != null){
                    that.displayedBlogs.push(that.helperArray[i]);
                } else {
                    break;
                }
                
                i +=1;
            };
        };

        // ---- PROFILE PAGE -----
        this.sendToProfile = function(){
            $http.get('api/author_id', {headers: {"Authorization": "Bearer " + that.token}}).then(
                function(response){
                    var id = response.data;
                    $state.go("profilePage", {"authorsId": id});
                }, function(reason){
                    console.log(reason);
                }
            )
        }


       // ----- POZIVI PO INICIJALIZACIJI -----
        this.getInitialContent();

    }])
})(angular, document)