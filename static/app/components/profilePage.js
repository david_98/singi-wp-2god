(function(angular, document){
    var app = angular.module("app");
    app.controller("profileController", ['$http', '$stateParams', '$state', 'authService', 'authorValidityService', 'tokenValidityService', function($http, $stateParams, $state, authService, authorValidityService, tokenValidityService){
        tokenValidityService("token not needed");
        var that = this;
        this.isAuthor = true;
        this.potentialEdits = false;

        this.authorsId = $stateParams['authorsId'];

        this.authorInfo = {
            profPic: '',
            author: '',
            about: ''
        };
        this.stats = {
            totalScore: 0,
            totalViews: 0
        };

        this.authorsBlogsPublished = [];
        this.authorsBlogsUnpublished = [];
        this.categories = [];
        this.token;

        this.setUpPage = function(authorsId){
            that.token = authService()
    
            if(that.token){
                $http.get("api/profile_page/validity", {"headers": {"Authorization": "Bearer " + that.token, "authorsId": authorsId}}).then(
                    function(response){
                        // provera i dobavljanje blogova
                        if(response.data['validity'] == "valid"){
                            that.isAuthor = true;
                            that.getDataIfValid(authorsId);
                        } else {
                            that.isAuthor = false;
                            that.getDataIfNotValid(authorsId);
                        }

                    }, function(reason){
                        console.log(reason);
                    }
                )
            } else {
                that.isAuthor = false;
                that.getDataIfNotValid(authorsId);
            }

        }

        this.getDataIfValid = function(authorsId){
            $http.get("api/profile_page/data/" + authorsId + "/valid").then(
                function(response){
                    // authorInfo i blogovi
                    that.authorInfo = response.data["authorInfo"];
                    that.authorsBlogsPublished = response.data["publishedBlogs"];
                    that.authorsBlogsUnpublished = response.data["unpublishedBlogs"];
                    that.calculateStats(that.authorsBlogsPublished);
                }, function(reason){
                    console.log(reason);
                }
            )
        }

        this.getDataIfNotValid = function(authorsId){
            $http.get("api/profile_page/data/" + authorsId + "/not_valid").then(
                function(response){
                    // authorInfo i blogovi
                    that.authorInfo = response.data["authorInfo"];
                    that.authorsBlogsPublished = response.data["publishedBlogs"];
                    that.calculateStats(that.authorsBlogsPublished);
                }, function(reason){
                    console.log(reason);
                }  
            )
        }

        this.offerSave = function(){
            that.potentialEdits = true;
        }

        // za STATS
        this.calculateStats = function(blogs){
            let views = 0;
            let score_number = 0;
            let score = 0;
            for (blog of blogs){
                // views
                views += blog["views"];
                // score
                score += blog["score_total"]
                score_number += blog['score_number'];
            }
            score = Math.round(score/score_number);
            if(isNaN(score)){
                score = 0;
            }
            that.stats["totalScore"] = score;
            that.stats["totalViews"] = views;
        }
        // SACUVAJ IZMENE
        this.saveChanges = function(){
            // cuvanje izmena nad imenom i opisom
            $http.patch("api/profile/edit", that.authorInfo, {headers: {"Authorization": "Bearer " + that.token}}).then(
                function(response){
                    console.log(response);
                }, function(reason){
                    console.log(reason);
                }
            );
            // provera da li je osoba promenila profilnu sliku
            if(document.getElementById("profImage").value){
                // kreiranje xml zahteva i form data
                var form = document.getElementById("authorsForm");
                var formData = new FormData(form);

                var request = new XMLHttpRequest();
            
                request.open("PATCH", "api/profile/edit_profile_picture")
                request.setRequestHeader("Authorization", "Bearer " + that.token);
                request.send(formData);
               
                request.onreadystatechange = function(){
                    if(request.readyState === 4){
                        that.authorInfo['profPic'] = request.response;
                        $state.reload();
                    }
                    
                };
            };

            that.potentialEdits = false;

        }

        this.getCategories = function(){
            $http.get("api/profile/categories").then(
                function(response){
                    that.categories = response.data;
                }, function(reason){
                    console.log(reason);
                }
            )
        }
        this.getScore = function(total, number){
            var score = Math.round(total/number);
            if(isNaN(score)){
                score = 0;
            }
            return score;
        }

        this.removeBlog = function(id, typeOfBlog){
            $http.put("api/profile/remove_blog/" + id, null, {headers: {"Authorization": "Bearer " + that.token}}).then(
                function(response){
                    console.log(response);
                    $state.reload().then(
                        () => {
                                if(typeOfBlog == "published"){
                                    document.getElementById("buttonPublishedBlogs").click();
                                } 
                                // ovo iz nekog razloga ne radi
                                // else {
                                //     document.getElementById("buttonUnpublishedBlogs").click();
                                // }
                        }
                    )
                    
                }, function(reason){
                    console.log(reason);
                }
            )
        };

        this.changeImage = function(){
            that.potentialEdits = true;
            document.getElementById("profImage").click();
            
        }
        
        // PO INICIJALIZACIJI
        this.setUpPage(this.authorsId);
        this.getCategories();
    }])
})(angular, document)