(function(angular){
    var app = angular.module('app');
    app.controller('errorController', ['$stateParams', function($stateParams){
        this.code = $stateParams['code'];
    }])
})(angular)