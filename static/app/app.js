(function(angular){
    var app = angular.module("app", ['ui.router', 'ngSanitize']);
    app.config(["$stateProvider", "$urlRouterProvider", function($stateProvider, $urlRouterProvider){
    
        var pocetna = {
            name: 'pocetna',
            url: '/',
            templateUrl: 'app/components/pocetna.html',
            controller: 'pocetnaController',
            controllerAs: 'pCtrl'
        }

        var auth = {
            name: 'auth',
            url: '/auth/{mode}', // ovde ce biti parametar za log in ili sign in
            templateUrl: 'app/components/authPage.html',
            controller: 'authController',
            controllerAs: 'authCtrl'
        }
        var blogPage = {
            name: 'blogPage',
            url: '/blog/{state}/{blog_id}',
            templateUrl: 'app/components/blogPage.html',
            controller: 'blogController',
            controllerAs: 'blogCtrl'
        }
        var profilePage = {
            name: 'profilePage',
            url: '/profile/{authorsId}', 
            templateUrl: 'app/components/profilePage.html',
            controller: 'profileController',
            controllerAs: 'profileCtrl'
        }
        var errorPage = {
            name: 'errorPage',
            url: '/errorPage/{code}',
            templateUrl: 'app/components/errorPage.html',
            controller: 'errorController',
            controllerAs: 'errorCtrl'
        }

        
        $stateProvider.state(pocetna);
        $stateProvider.state(auth);
        $stateProvider.state(blogPage);
        $stateProvider.state(errorPage);
        $stateProvider.state(profilePage);


        $urlRouterProvider.otherwise("/");
    }])
})(angular)