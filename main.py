import os
import json
import flask
import datetime
from flask import Flask, request, flash, redirect, url_for, render_template
from werkzeug.utils import secure_filename
from werkzeug.wrappers import Request, Response
from werkzeug.datastructures import FileStorage
# from flask import flask_uploads
# from flask_uploads import UploadSet, configure_uploads, IMAGES
import flask_jwt_extended
from flask_jwt_extended import JWTManager, jwt_required, create_access_token, get_jwt_identity, get_jwt_claims


# Dobavljanje mysql promenljive iz db modula.
from utils.db import mysql


# Dobavljanje blueprint-ova.
from blueprints.auth import auth_blueprint
from blueprints.blog import blog_blueprint
from blueprints.profile import profile_blueprint


app = Flask(__name__, static_url_path="")
app.config["MYSQL_DATABASE_USER"] = "root" # Korisnicko ime.
app.config["MYSQL_DATABASE_PASSWORD"] = "root" # Lozinka.
app.config["MYSQL_DATABASE_DB"] = "blogist" # Naziv seme baze podataka na koju se treba povezati

app.config["JWT_SECRET_KEY"] = "wp"
mysql.init_app(app)
jwt = JWTManager(app)

# Registrovanje blueprint-a.
app.register_blueprint(auth_blueprint, url_prefix="/api")
app.register_blueprint(blog_blueprint, url_prefix="/api")
app.register_blueprint(profile_blueprint, url_prefix="/api")


# dodavanje id korisnika u token
@jwt.user_claims_loader
def add_claims_to_access_token(identity):
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT id FROM author WHERE author=%s", (identity, ))
    rezultat = cursor.fetchone()
    print(rezultat)
    return {
        'author': identity,
        'id': rezultat['id']
    }



@app.route("/")
@app.route("/index")
def index_page():
    return app.send_static_file("index.html")

# @Request.application
@app.route("/api/createBlog", methods=["POST"])
def createBlog():

    request.files["media"].save("static/mesto/" + request.files["media"].filename + "", buffer_size=16384)

    return ""

@app.route("/api/getImage")
def sendImgSrc():
    return flask.jsonify("mesto/048-750x300.jpg")

# -- ZA POCETNA --

@app.route("/api/get_initial_content", methods=["GET"])
def get_blogs():
    db = mysql.get_db()
    cursor = db.cursor()
    cursor.execute("SELECT * FROM blog WHERE state=1")
    results_blogs = cursor.fetchall()
    cursor.execute("SELECT * FROM content")
    results_content = cursor.fetchall()
    cursor.execute("SELECT * FROM category")
    results_categories = cursor.fetchall()
    cursor.execute("SELECT * FROM author")
    results_authors = cursor.fetchall()
    return flask.jsonify({"blogs": results_blogs, "content": results_content, "categories": results_categories, "authors": results_authors})

@app.route("/api/get_initial_content/<sort_type>")
def get_blogs_sorted(sort_type):
    db = mysql.get_db()
    cursor = db.cursor()
    if sort_type == "newBlogs":
        cursor.execute("SELECT * FROM blog ORDER BY date DESC")
        results = cursor.fetchall()
        return flask.jsonify(results)
    if sort_type == "bestRated":
        cursor.execute("SELECT * FROM blog ORDER BY score_total/score_number DESC")
        results = cursor.fetchall()
        return flask.jsonify(results)
    if sort_type == "popularBlogs":
        cursor.execute("SELECT * FROM blog ORDER BY views DESC")
        results = cursor.fetchall()
        return flask.jsonify(results)
    
@app.route("/api/author_id", methods=['GET'])
@jwt_required
def get_authors_id():
    claims = get_jwt_claims()
    id = claims['id']
    return flask.jsonify(id), 200

# -- ZA AUTHOR VALIDITY SERVICE
@app.route("/api/author_validity", methods=["GET"])
@jwt_required
def check_if_author():
    claims = get_jwt_claims()
    users_id = claims["id"]
    blog_id = request.headers["blog_id"]
    db = mysql.get_db()
    cursor = db.cursor()
    # trazim id autora bloga sa tim id
    cursor.execute("SELECT * FROM blog WHERE id=%s", (blog_id, ))
    result1 = cursor.fetchone()
    authors_id = result1["authors_id"]
    print(users_id)
    print(authors_id)
    if users_id == authors_id:
        return flask.jsonify({"access": True})
    else: 
        return flask.jsonify({"access": False})

if __name__ == "__main__":

    app.run("0.0.0.0", 5000, threaded=True)

# Nakon pokretanja aplikacije u web browser je moguce
# uneti URL http://localhost:5000 i videti rezultat ove skripte.